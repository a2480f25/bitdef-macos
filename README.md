Să se scrie un produs software pentru Mac OS care va servi, într-un mediu Enterprise, la evidența de Stick-uri de memorie USB (și posibil alte medii de stocare externe). Concret, evidența va fi ținută pentru volumele care pot fi montate de pe stick-uri.
 
Cerințe ce derivă din natura “Enterprise”
stick-urile pot fi transferate între calculatoare diferite
vor fi stick-uri formatate pe un Mac, dar și stick-uri Windows
 
Descriere generală
	Se va scrie un client care ruleaza local, pe mașinile cu MacOS. Din cauza mutării stick-urilor de la o mașină la alta, va exista și un server de management prin care se sincronizeaza programele client de pe diferitele mașini. Practic serverul este cel care va ține evidența stick-urilor. Aplicația client va realiza și citirea datelor legate de stickurile conectate la sistem și va oferi și o interfață grafică.
	
Descriere funcțională
	La pornire clientul local va efectua o sincronizare cu serverul și va afișa lista de stick-uri cunoscute (până în acel moment), inclusiv stick-urile conectate la mașină.
	La introducerea unui stick, se vor citi informațiile despre volumele care pot fi montate. Ele vor fi afișate în clientul local (GUI) și vor fi sincronizate cu serverul. Dacă volumele nu au mai apărut în evidență, vor fi colorate cu roșu. Cele care au mai apărut, vor fi afișate cu verde (iar celelalte stick-uri, care nu sunt conectate la sistem, vor fi cu negru).
	Datele citite pentru un volum sunt ID-ul și numele volumului.
	Se vor mai oferi:
un buton de refresh (efectuează o resincronizare cu serverul)
un buton de delete: șterge din server (și lista locală) volumul selectat. Această operație este posibilă doar dacă stick-ul aferent nu este introdus în calculator
 
Descrierea protocolului
Comunicarea este bazată pe TCP/IP și constă din schimburi de comenzi / răspunsuri în format JSON.
Fiecare obiect de comunicare are o proprietate ce conține comanda și o proprietate cu datele asociate.
Fiecare răspuns are un câmp care arată dacă s-a efectuat comanda cu succes sau a apărut o eroare și răspunsul la comandă (când este cazul).
 
Comenzile sunt:

```
#!json

CONNECT – prima comunicare după conectare
{
	“cmd”=”connect”,
	“data”=null
}
{
	“cmd”=”connect”,
	“error”=0,
	“data”=null
}
GET – citește lista de volume de pe server
{
	“cmd”=”get”,
	“data”=null
}
{
	“cmd”=”get”,
	“error”:0,
	“data”: [
		{
			“volId”:”ID1”,	
			“volName”:”name1”
		},
		{
			“volId”:”IDStick2”,
			“volName”:”Stick de serviciu”
		},…
	]
}
ADD – salvează în server un volum
{
	“cmd”=”add”,
	“data”= {
		“volId”:”id5”,
		“volName”:”StickNou”
	}
}
{
	“cmd”=”add”,
	“error”:0,
	“data”: null
}
DELETE – șterge un volum de pe server
{
	“cmd”=”delete”,
	“data”= {
		“volId”:”id5”,
		“volName”:”StickNou”
	}
}
{
	“cmd”=”delete”,
	“error”:0,
	“data”: null
}
DISCONNECT – sfârșitul comunicării
{
	“cmd”=”disconnect”,
	“data”=null
}
{
	“cmd”=”disconnect”,
	“error”=0,
	“data”=null
}
```

 
Prima comanda este obligatoriu cea de conectare. Serverul nu accepta alte comenzi într-o sesiune până ce nu s-a efectuat conectare (câmpul eroare va avea valoare nenulă). După deconectare, socketul va fi închis.
 
Cerințe de implementare
se va implementa doar aplicația client, serverul va fi furnizat
aplicația va fi implementată în Objective-C (a se vedea diskarbitration)
nu se pune accent pe gradul de finisare a interfeței grafice
se pune accent major pe experiența utilizatorului: nu se admit crash-uri, agățări ale UI etc.