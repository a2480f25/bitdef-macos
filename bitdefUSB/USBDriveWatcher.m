//
//  USBDriveWatcher.m
//  bitdefUSB
//
//  Created by Good on 15/05/2017.
//  Copyright © 2017 Good. All rights reserved.
//

#import <CommonCrypto/CommonDigest.h>

#import "DiskRecord.h"
#import "USBDriveWatcher.h"

@implementation USBDriveWatcher

- (id) init {
    _disks      = [NSMutableArray new];
    _diskLock   = [NSLock new];
    [self performSelectorInBackground:@selector(detectUSB:) withObject:self];
    
    return self;
}

-(NSMutableArray*) getDisks {
    NSMutableArray* copy;
    [_diskLock lock];
    copy = [_disks copy];
    [_diskLock unlock];
    return copy;
}

NSDictionary* get_disk_dict(DADiskRef disk,  void* context) {
    CFDictionaryRef description = DADiskCopyDescription(disk);
    NSString* identifier = _DiskIdentifierFromDiskDescription((__bridge NSDictionary*)description);
    NSString* bsdName = [NSString stringWithCString:DADiskGetBSDName(disk) encoding:NSUTF8StringEncoding];
    
    
    NSURL *volumeURL = (__bridge NSURL*) CFDictionaryGetValue(description, kDADiskDescriptionVolumePathKey);
    if (!volumeURL) {
        volumeURL = [NSURL URLWithString:@""];
    }
    
    NSFileManager* fm = [NSFileManager alloc];
    NSNumber* diskSize = @0;
    NSNumber* freeSpace = @0;
    
    NSNumber* writable = @false;
    if (volumeURL.path) {
        if([fm isWritableFileAtPath:volumeURL.path]) {
            writable = @true;
        }
        NSDictionary* attributes = [fm attributesOfFileSystemForPath: volumeURL.path error:nil];
        
        if(attributes) {
            diskSize = [attributes objectForKey:NSFileSystemSize];
            freeSpace = [attributes objectForKey:NSFileSystemFreeSize];
        }
        NSLog(@"New USB Disk %s is mounted at %@", DADiskGetBSDName(disk), volumeURL.path);
        USBDriveWatcher* watcher = (__bridge USBDriveWatcher*) context;

        [[NSNotificationCenter defaultCenter] postNotificationName:@"numberOfSticksUpdated"
                                                            object:nil
                                                          userInfo:@{@"info":
                                                                         [NSNumber numberWithUnsignedInteger:[watcher.disks count]]}];
    } else {
        NSLog(@"Disk is NOT mounted");
    }
    
    NSString *volumeName = CFDictionaryGetValue(description, kDADiskDescriptionVolumeNameKey);
    NSString *uuidkey = CFDictionaryGetValue(description, kDADiskDescriptionVolumeUUIDKey);
    NSString *mediaName = CFDictionaryGetValue(description, kDADiskDescriptionMediaNameKey);
    NSString *mediaUUIDkey = CFDictionaryGetValue(description, kDADiskDescriptionMediaUUIDKey);
    NSString *deviceGUIDKey = CFDictionaryGetValue(description, kDADiskDescriptionDeviceGUIDKey);
    
 
    NSDictionary* diskRecord = @{
                                 @"identifier": identifier == nil? @"" : identifier,
                                 @"uuidKey" : uuidkey == nil? @"" : uuidkey,
                                 @"mediaName" : mediaName == nil? @"" : mediaName,
                                 @"mediaUUIDKey" : mediaUUIDkey == nil? @"" : mediaUUIDkey,
                                 @"deviceGUIDKey" : deviceGUIDKey == nil? @"" : deviceGUIDKey,
                                 @"bsdName": bsdName,
                                 @"name": volumeName,
                                 @"volumeURL": volumeURL,
                                 @"path": (volumeURL.path?volumeURL.path:@""),
                                 @"mounted": (volumeURL.path?@true:@false),
                                 @"writable": writable,
                                 @"diskSize": diskSize,
                                 @"freeSpace": freeSpace};
    
    CFRelease(description);
    
    return diskRecord;
}

void disk_did_appear(DADiskRef disk, void* context) {
    USBDriveWatcher* watcher = (__bridge USBDriveWatcher*) context;
    
    NSDictionary* diskRecord = get_disk_dict(disk, context);
    
    [watcher.diskLock lock];
    [watcher.disks addObject:diskRecord];
    [watcher.diskLock unlock];
    
    //Mount new disk, in case it is not already mounted..
    DADiskMount(disk, NULL, 0, NULL, NULL);
    //DADiskUnmount(disk, kDADiskUnmountOptionForce, NULL, NULL);
    
    if ([diskRecord objectForKey:@"writable"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"writableDiskArrived" object:nil];
    }
    DiskRecord* d = [[DiskRecord alloc] initWithDictionary:diskRecord];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"newStickArrived" object:nil userInfo:@{@"info":d}];

    NSLog(@"A total of %lu USB drives now present", (unsigned long)[watcher.disks count]);
}

void disk_params_changed(DADiskRef disk, CFArrayRef keys, void * context) {
    CFDictionaryRef description = DADiskCopyDescription(disk);
    USBDriveWatcher* watcher = (__bridge USBDriveWatcher*) context;
    NSString* bsdName = [NSString stringWithCString:DADiskGetBSDName(disk) encoding:NSUTF8StringEncoding];
    
    NSDictionary* diskRecord = get_disk_dict(disk, context);
    
    for (int i = 0; i < CFArrayGetCount(keys); i++) {
        
        if (CFStringCompare(CFArrayGetValueAtIndex(keys, i), kDADiskDescriptionVolumePathKey, 0) == kCFCompareEqualTo) {
            NSURL *volumeURL = (__bridge NSURL*) CFDictionaryGetValue(description, kDADiskDescriptionVolumePathKey);
            if (volumeURL) {
                [watcher.diskLock lock];
                for (NSDictionary* disk in [watcher disks]) {
                    if ([[disk objectForKey:@"bsdName"] isEqualToString:bsdName]) {
                        [[watcher disks] setObject:diskRecord atIndexedSubscript:[[watcher disks] indexOfObject:disk]];
                        break;
                    }
                }
                [watcher.diskLock unlock];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"writableDiskArrived" object:nil];
            }
        }
    }
    CFRelease(description);
}

void disk_did_disappear(DADiskRef disk, void* context) {
    USBDriveWatcher* watcher =  (__bridge USBDriveWatcher*)context;
    NSString* name = [NSString stringWithCString:DADiskGetBSDName(disk) encoding:NSUTF8StringEncoding];
    
    NSLog(@"Disk %@ was removed!", name);
    
    [watcher.diskLock lock];
    NSDictionary* removedDisk;
    for (NSDictionary* disk in watcher.disks) {
        if ([[disk objectForKey:@"bsdName"] isEqualToString:name]) {
            removedDisk = disk;
            goto found;
        }
    }
    NSLog(@"ERROR: Could not find record of removed device!");
found:
    if(removedDisk){
        [watcher.disks removeObject:removedDisk];
    }
    NSLog(@"A total of %lu USB drives now present", (unsigned long)[watcher.disks count]);
    [watcher.diskLock unlock];
    DiskRecord* d = [[DiskRecord alloc] initWithDictionary:removedDisk];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"diskRemoved" object:nil userInfo:@{@"info":d}];
}

- (void) unmountDrives {
    DASessionRef session;
    session = DASessionCreate(kCFAllocatorDefault);
    
    [_diskLock lock];
    for (NSDictionary* disk in _disks) {
        const char* bsdName = [[disk objectForKey:@"bsdName"] cStringUsingEncoding:NSUTF8StringEncoding];
        DADiskRef disk = DADiskCreateFromBSDName(kCFAllocatorDefault, session, bsdName);
        DADiskUnmount(disk, 0, NULL, NULL);
        CFRelease(disk);
    }
    [_diskLock unlock];
    
    CFRelease(session);
}

- (void) mountDrives {
    DASessionRef session;
    session = DASessionCreate(kCFAllocatorDefault);
    
    [_diskLock lock];
    for (NSDictionary* disk in _disks) {
        const char* bsdName = [[disk objectForKey:@"bsdName"] cStringUsingEncoding:NSUTF8StringEncoding];
        DADiskRef disk = DADiskCreateFromBSDName(kCFAllocatorDefault, session, bsdName);
        DADiskMount(disk, NULL, 0, NULL, NULL);
        CFRelease(disk);
    }
    [_diskLock unlock];
    
    CFRelease(session);
}

DASessionRef _session;

- (void) detectUSB: (USBDriveWatcher*) parent {
    
    _session = DASessionCreate(kCFAllocatorDefault);
    
    CFMutableDictionaryRef keys = CFDictionaryCreateMutable(kCFAllocatorDefault, 0, NULL, NULL);
    CFDictionaryAddValue(keys, kDADiskDescriptionMediaRemovableKey, kCFBooleanTrue);
    CFDictionaryAddValue(keys, kDADiskDescriptionMediaWritableKey, kCFBooleanTrue);
    CFDictionaryAddValue(keys, kDADiskDescriptionVolumeMountableKey, kCFBooleanTrue);
    CFDictionaryAddValue(keys, kDADiskDescriptionDeviceProtocolKey, CFSTR("USB"));
    CFDictionaryAddValue(keys, kDADiskDescriptionDeviceInternalKey, kCFBooleanFalse);
    
    void *context = (__bridge void*) parent;
    
    DARegisterDiskAppearedCallback(_session,keys,disk_did_appear, context);
    DARegisterDiskDisappearedCallback(_session, keys, disk_did_disappear, context);
    DARegisterDiskDescriptionChangedCallback(_session, keys, NULL, disk_params_changed, context);
    
    DASessionScheduleWithRunLoop(_session, CFRunLoopGetMain(), kCFRunLoopDefaultMode);
    
    //DASessionSetDispatchQueue(_session, dispatch_get_global_queue(0, 0));
    
    CFRelease(keys);
}

- (void) dealloc {
    [self mountDrives];
    
    DASessionUnscheduleFromRunLoop(_session, CFRunLoopGetMain(), kCFRunLoopDefaultMode);
    CFRelease(_session);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"diskRemoved" object:nil];
    NSLog(@"USB Dealloc");
}

static NSString* _DiskIdentifierFromDiskDescription(NSDictionary* description)
{
    NSString*				string = nil;
    CFUUIDRef				uuid;
    NSMutableData*			data;
    unsigned char			md5[16];
    
    uuid = (__bridge CFUUIDRef)[description objectForKey:(id)kDADiskDescriptionVolumeUUIDKey];
    if(uuid)
        string = (id)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, uuid));
    
    if(string == nil) {
        uuid = (__bridge CFUUIDRef)[description objectForKey:(id)kDADiskDescriptionMediaUUIDKey];
        if(uuid)
            string = (id)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, uuid));
    }
    
    if(string == nil) {
#ifdef __DEBUG__
        string = [description objectForKey:(id)kDADiskDescriptionVolumeNameKey];
        if(string == nil)
            string = [description objectForKey:(id)kDADiskDescriptionMediaNameKey];
        NSLog(@"%s: DADiskCopyDescription() contains no UUID for \"%@\"", __FUNCTION__, string);
#endif
        NSLog(@"%s: DADiskCopyDescription() contains no UUID for \"%@\"", __FUNCTION__, string);
        data = [NSMutableData new];
        _AppendToData(data, [description objectForKey:(id)kDADiskDescriptionVolumeKindKey]);
        _AppendToData(data, [description objectForKey:(id)kDADiskDescriptionVolumeNameKey]);
        _AppendToData(data, [description objectForKey:(id)kDADiskDescriptionMediaNameKey]);
        _AppendToData(data, [description objectForKey:(id)kDADiskDescriptionDeviceVendorKey]);
        _AppendToData(data, [description objectForKey:(id)kDADiskDescriptionDeviceModelKey]);
        _AppendToData(data, [description objectForKey:(id)kDADiskDescriptionDeviceProtocolKey]);
        _AppendToData(data, [description objectForKey:(id)kDADiskDescriptionMediaSizeKey]);
        CC_MD5([data mutableBytes], [data length], md5);
        //[data release];
        string = [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X", md5[0], md5[1], md5[2], md5[3], md5[4], md5[5], md5[6], md5[7], md5[8], md5[9], md5[10], md5[11], md5[12], md5[13], md5[14],md5[15]];
    }
    
    return string;
}

static inline void _AppendToData(NSMutableData* data, id value)
{
    [data appendData:[[value description] dataUsingEncoding:NSUTF8StringEncoding]];
}
@end
