//
//  ViewController.h
//  bitdefUSB
//
//  Created by Good on 15/05/2017.
//  Copyright © 2017 Good. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <AppKit/AppKit.h>

#import "USBDriveWatcher.h"
#import "GCDAsyncSocket.h"

@interface ViewController : NSViewController <NSTableViewDelegate, NSTableViewDataSource, GCDAsyncSocketDelegate> {
    GCDAsyncSocket *asyncSocket;
    GCDAsyncSocket *connectedSocket;
}

@property USBDriveWatcher* usbDriveWatcher;

@end

