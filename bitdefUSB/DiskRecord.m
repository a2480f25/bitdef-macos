//
//  DiskRecord.m
//  bitdefUSB
//
//  Created by Good on 17/05/2017.
//  Copyright © 2017 Good. All rights reserved.
//

#import "DiskRecord.h"

@interface DiskRecord()

@end

@implementation DiskRecord
- (instancetype) initWithDictionary:(NSDictionary *)d {
    if (self = [super init]) {
        self.identifier     = d[@"identifier"];
        self.uuidKey        = d[@"uuidKey"];
        self.mediaName      = d[@"mediaName"];
        self.mediaUUIDKey   = d[@"mediaUUIDKey"];
        self.deviceGUIDKey  = d[@"deviceGUIDKey"];
        self.bsdName        = d[@"bsdName"];
        self.name           = d[@"name"];
        self.volumeURL      = d[@"volumeURL"];
        self.path           = d[@"path"];
        self.mounted        = [d[@"mounted"] boolValue];
        self.writable       = d[@"writable"];
        self.diskSize       = d[@"diskSize"];
        self.freeSpace      = d[@"freeSpace"];
    }
    return self;
}

- (instancetype) initWithRemoteDictionary:(NSDictionary *)d {
    if (self = [super init]) {
        self.identifier = d[@"volID"];
        self.name       = d[@"volName"];
    }
    return self;
}

+ (void) addUniqueDiskRecord:(DiskRecord *)r toArray:(NSMutableArray *)a{
    bool found = false;
    
    for (DiskRecord *d in a) {
        if ([self isSameObject:d with:r]) {
            found = true;
            break;
        }
    }
    
    if (!found) {
        [a addObject:r];
    }
    
}

+ (BOOL) isObject:(DiskRecord *)r presentIn:(NSMutableArray *)a {
    bool found = false;
    
    for (DiskRecord *remote in a) {
        if ([self isSameObject:remote with:r]) {
            found = true;
            break;
        }
    }
    
    return found;
}

+ (void) removeObject:(DiskRecord *)rr fromArray:(NSMutableArray *)a {
    int i = 0;
    for (DiskRecord* d in a) {
        if ([self isSameObject:d with:rr]) {
            [a removeObjectAtIndex:i];
            break;
        }
        i++;
    }
}

+ (DiskRecord *) findDiskRecordWithName:(NSString *)s inArray:(NSArray *)a {
    DiskRecord *r;
    for (DiskRecord *dr in a) {
        if ([dr.name.lowercaseString isEqualToString:s.lowercaseString]) {
            r = dr;
            break;
        }
    }
    return r;
}

//should be only id not name. server bug workaround.
+ (BOOL)isSameObject :(DiskRecord *)a with:(DiskRecord *)b {
    bool byIdentifier = [a.identifier.lowercaseString isEqualToString:b.identifier.lowercaseString];
    bool byName = [a.name.lowercaseString isEqualToString:b.name.lowercaseString];
    bool nullId = ![a.identifier.lowercaseString isEqualToString:@"null"] && ![b.identifier.lowercaseString isEqualToString:@"null"];
    
    return (byIdentifier && nullId) || byName;
}

@end
