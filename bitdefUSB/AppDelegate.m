//
//  AppDelegate.m
//  bitdefUSB
//
//  Created by Good on 15/05/2017.
//  Copyright © 2017 Good. All rights reserved.
//

#import "AppDelegate.h"
#import "SocketManager.h"
#import "USBDriveWatcher.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
    [[SocketManager sharedInstance] disconnectFromServer:^(NSDictionary *data, NSError *error) {
        //
    } :^(NSError *error) {
        //
    }];
}


@end
