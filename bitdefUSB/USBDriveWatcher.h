//
//  USBDriveWatcher.h
//  bitdefUSB
//
//  Created by Good on 15/05/2017.
//  Copyright © 2017 Good. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <DiskArbitration/DiskArbitration.h>

@interface USBDriveWatcher : NSObject {
    NSMutableArray* _disks;
}

- (id) init;
- (NSMutableArray*) getDisks;
- (void) unmountDrives;
- (void) mountDrives;

@property (copy) NSMutableArray* disks;
@property NSLock* diskLock;
@end
