//
//  SocketManager.h
//  bitdefUSB
//
//  Created by Good on 18/05/2017.
//  Copyright © 2017 Good. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCDAsyncSocket.h"
#import "DiskRecord.h"

@interface SocketManager : NSObject

- (instancetype) init __attribute__((unavailable("init not available")));

+ (id)sharedInstance ;
- (void) connectToServer:(void (^)(NSDictionary* data, NSError* error))completionHandler
                        :(void (^)(NSError* error))completionHandlerDisconnect;

- (void) fetchDataFromServer:(void (^)(NSDictionary* data,NSError* error))completionHandler
                            :(void (^)(NSError* error))completionHandlerDisconnect;

- (void) addDataToServer:(DiskRecord *)data
                        :(void (^)(NSDictionary* data,NSError* error))completionHandler
                        :(void (^)(NSError* error))completionHandlerDisconnect;

- (void) disconnectFromServer:(void (^)(NSDictionary* data,NSError* error))completionHandler
                             :(void (^)(NSError* error))completionHandlerDisconnect;

- (void)deleteDiskRecordFromServer: (DiskRecord *)data
                                  :(void (^)(NSDictionary* data,NSError* error))completionHandler
                                  :(void (^)(NSError* error))completionHandlerDisconnect;
@end
