//
//  ViewController.m
//  bitdefUSB
//
//  Created by Good on 15/05/2017.
//  Copyright © 2017 Good. All rights reserved.
//

#import "ViewController.h"
#import <Foundation/Foundation.h>
#import <DiskArbitration/DiskArbitration.h>

#import "DiskRecord.h"
#import "GCDAsyncSocket.h"
#import "SocketManager.h"

@interface ViewController()
@property (strong) IBOutlet NSTableView *tableView;
@property NSInteger selectedRow;

@property (strong) NSMutableArray* tableViewDataSourceLocal;
@property (strong) NSMutableArray* tableViewDataSourceRemote;
@property (strong) NSMutableArray* tableViewDataSourceMerged;
@property BOOL shouldDeselectTableViewRow;

@property (strong) IBOutlet NSTextField *statusTextField;

@property (strong) SocketManager* socketManager;

@property (strong) IBOutlet NSButton *startMonitoringButton;

@property (strong) IBOutlet NSButton *stopMonitoringButton;
@property (strong) IBOutlet NSButton *refreshButton;

@property (strong) IBOutlet NSButton *deleteButton;
@property (strong) IBOutlet NSButton *addButton;

@end

@implementation ViewController 

- (void)viewDidLoad {
    [super viewDidLoad];

    _socketManager = [SocketManager sharedInstance];
    _selectedRow = -1;
    _tableViewDataSourceLocal = [NSMutableArray new];
    _tableViewDataSourceRemote = [NSMutableArray new];
    _tableViewDataSourceMerged = [NSMutableArray new];
    _shouldDeselectTableViewRow = TRUE;
    
    [self registerForNotifications];
    [self startUSBWatcher];
    [self connectToServer];
    [self setButtonStates:TRUE];
}

#pragma mark - UI

- (IBAction)stopMonitoring:(NSButton *)sender {
    [self stopUSBWatcher];
    [self disconnectFromServer];
    [self setButtonStates:FALSE];
    
    [_tableViewDataSourceMerged removeAllObjects];
    [_tableView reloadData];
}

- (IBAction)startMonitoring:(id)sender {
    [self startUSBWatcher];
    [self connectToServer];
    [self setButtonStates:TRUE];
}

- (IBAction)refreshData:(NSButton *)sender {
    [self getDataFromServer];
}

- (IBAction)deleteSelected:(NSButton *)sender {
    if (_selectedRow == -1) {
        _statusTextField.stringValue = @"Delete: Please select row in table view above.";
        return;
    }

    [self deleteSelectedFromServer];
}

- (IBAction)addSelected:(NSButton *)sender {
    if (_selectedRow == -1) {
        _statusTextField.stringValue = @"Add: Please select row in table view above.";
        return;
    }
    
    [self addSelectedToServer];
}

- (void)setButtonStates:(BOOL)a {
    [_startMonitoringButton     setEnabled:!a];
    [_stopMonitoringButton      setEnabled:a];
    [_refreshButton             setEnabled:a];
    [_deleteButton              setEnabled:a];
    [_addButton                 setEnabled:a];
}

#pragma mark - USB

-(void) startUSBWatcher {
    if(!_usbDriveWatcher) {
        _usbDriveWatcher = [[USBDriveWatcher alloc] init];
    }
}

-(void) stopUSBWatcher {
    _usbDriveWatcher = nil;
}

#pragma mark - Server Operations

- (void) connectToServer {
    _statusTextField.stringValue = @"Connecting to server...";
    [_socketManager connectToServer:^(NSDictionary *data, NSError *error) {
        if (error) {
            [self printMessage:@"Server Connection failure." forError:error];
            return;
        }
        [self printMessage:@"Connected to server." forError:error];
        [self getDataFromServer];
        
    } :^(NSError *error) {
        [self handleDisconnectionError:error];
    }
    ];
}

- (void) getDataFromServer {
    _statusTextField.stringValue = @"Retrieving data from server...";
    [_socketManager fetchDataFromServer:^(NSDictionary *data, NSError *error) {
        if (error) {
            [self printMessage:@"Server fetch failure. Refresh." forError:error];
            return;
        }
        
        NSLog(@"from main: %@",data.description);
        [self parseDataFromServer:data];
        _statusTextField.stringValue = @"Fetched from server.";
        
    } :^(NSError *error) {
        [self handleDisconnectionError:error];
    }];
}

- (void) disconnectFromServer {
    _statusTextField.stringValue = @"Disconnecting from server...";
    [_socketManager disconnectFromServer:^(NSDictionary *data, NSError *error) {
        if (error != nil) {
            [self printMessage:[NSString stringWithFormat:@"Disconnecting from server failed with error: %@", error.description] forError:error];
            return;
        }
        _statusTextField.stringValue = @"Finished disconnecting from server.";
    } :^(NSError *error) {
        [self handleDisconnectionError:error];
    }];
}

- (void) deleteSelectedFromServer {
    _statusTextField.stringValue = @"Deleting from server..";
    DiskRecord *rr = (DiskRecord *)(_tableViewDataSourceMerged[_selectedRow]);
    bool isUSBConnected = [self isLocalObject:rr];
    if (isUSBConnected) {
        _statusTextField.stringValue = @"Disconnect USB and try again del.";
        return;
    }
    
    [_socketManager deleteDiskRecordFromServer:rr :^(NSDictionary *data, NSError *error) {
         _selectedRow = -1;
        if (error != nil) {
            [self printMessage:[NSString stringWithFormat:@"Deleting data from server failed with error: %@", error.description] forError:error];
            return;
        }
        [DiskRecord removeObject:rr fromArray:_tableViewDataSourceRemote];
        [DiskRecord removeObject:rr fromArray:_tableViewDataSourceMerged];
       
        [_tableView reloadData];
        _statusTextField.stringValue = @"Finished deleting data from server.";

    } :^(NSError *error) {
        _selectedRow = -1;
        [self handleDisconnectionError:error];
    }];
    
}

- (void) addSelectedToServer {
    _statusTextField.stringValue = @"Adding to server...";
    DiskRecord *r = (DiskRecord *)(_tableViewDataSourceMerged[_selectedRow]);
    bool foundRemote = [self isRemoteObject:r];
    bool foundLocal = [self isLocalObject:r];
    
    if (foundLocal && foundRemote) {
        _statusTextField.stringValue = @"Found the id locally and remotely, no need to sync.";
    } else if (foundLocal && !foundRemote) {
        _statusTextField.stringValue = @"Adding element to remote..";
        
        [self addData:r];
    } else if (!foundLocal && foundRemote ) {
        _statusTextField.stringValue = @"Found it on the remote server, no need to sync.";
        
    } else if (!foundLocal && !foundRemote ) {
        _statusTextField.stringValue = @"Disconnected locally, adding to remote..";
        [self addData:r];
    }
}

- (void) addData:(DiskRecord *)r {
    __weak typeof(self) weakSelf = self;
    [_socketManager addDataToServer:r :^(NSDictionary *data, NSError *error) {
        if (error) {
            [weakSelf printMessage:[NSString stringWithFormat:@"Adding data from server failed with error: %@", error.description] forError:error];
            return;
        }
        if ([data[@"error"] integerValue]) {
            [weakSelf printMessage:[NSString stringWithFormat:@"Adding data from server failed."] forError:nil];
            return;
        }
        weakSelf.statusTextField.stringValue = @"Successfully added data to server. Refresh to resync.";
    } :^(NSError *error) {
        [weakSelf handleDisconnectionError:error];
    }];
}

- (void)handleDisconnectionError:(NSError *)error {
    if (error != nil) {
        if (error.domain == NSPOSIXErrorDomain && error.code == 2) {
            [self printMessage:@"Cannot connect to server, offline." forError:error];
            return;
        }
        [self printMessage:[NSString stringWithFormat:@"Disconnecting from server failed with error: %@", error.description] forError:error];
        return;
    } else {
        _statusTextField.stringValue = @"Finished disconnecting from server.";
    }
}

- (void)printMessage:(NSString *)m forError:(NSError *)error {
    if (error) {
        NSLog(@"error: %@",error.localizedDescription);
    }
    if (m) {
        _statusTextField.stringValue = m;
    }
}

#pragma mark - Parse

- (void) parseDataFromServer:(NSDictionary *)data {
    if (!data) {
        return;
    }
    NSUInteger error = data != nil ? [data[@"error"] integerValue] : 0;
    if (!error) {
        NSArray *arr = data[@"data"] != nil ? data[@"data"] : nil; //arr of dictionaries
        if (!arr || [arr isEqual:[NSNull null]]) {
            _statusTextField.stringValue = @"Error parsing data from server, please refetch";
            return;
        }
        if (arr && ![arr isEqual:[NSNull null]] && [arr count]) {
            NSLog(@"array count: %lu",(unsigned long)[arr count]);
            for (NSDictionary * d in arr) {
                DiskRecord *r = [[DiskRecord alloc] initWithRemoteDictionary:d];
                [DiskRecord addUniqueDiskRecord:r toArray:_tableViewDataSourceRemote];
                [DiskRecord addUniqueDiskRecord:r toArray:_tableViewDataSourceMerged];
                
                NSLog(@"volID:%@ - volNamee:%@", d[@"volID"], d[@"volName"]);
            }
            _shouldDeselectTableViewRow = NO;
            [_tableView reloadData];
        }
        
    }
}

- (BOOL) isLocalObject:(DiskRecord *)r {
    return [DiskRecord isObject:r presentIn:_tableViewDataSourceLocal];
}

- (BOOL) isRemoteObject:(DiskRecord *)r {
    return [DiskRecord isObject:r presentIn:_tableViewDataSourceRemote];
}

#pragma mark - Notifications
- (void) registerForNotifications {
    NSNotificationCenter* def = [NSNotificationCenter defaultCenter];
    [def addObserver:self selector:@selector(newStickArrivedNotification:) name:@"newStickArrived" object:nil];
    [def addObserver:self selector:@selector(diskRemovedNotification:) name:@"diskRemoved" object:nil];
}

- (void)newStickArrivedNotification:(NSNotification *)note {
    DiskRecord *r = (DiskRecord *)note.userInfo[@"info"];
    [DiskRecord addUniqueDiskRecord:r toArray:_tableViewDataSourceLocal];
    [DiskRecord addUniqueDiskRecord:r toArray:_tableViewDataSourceMerged];
    
    NSLog(@"%@", r.identifier);
    _shouldDeselectTableViewRow = NO;
    [self.tableView reloadData];
}

- (void)diskRemovedNotification:(NSNotification *)note {
    DiskRecord* dr = (DiskRecord *)(note.userInfo[@"info"]);
    [DiskRecord removeObject:dr fromArray:_tableViewDataSourceLocal];
    _shouldDeselectTableViewRow = NO;
    [_tableView reloadData];
}

#pragma mark - TableView

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView {
    return [_tableViewDataSourceMerged count];
}

- (nullable NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(nullable NSTableColumn *)tableColumn row:(NSInteger)row {
    NSString* cellId = @"NameCellID";
    NSTableCellView* cellView = [tableView makeViewWithIdentifier:cellId owner:self];

    DiskRecord *r = _tableViewDataSourceMerged[row];
    if ([r.identifier.lowercaseString isEqualToString:@"null"]) {
        r = [DiskRecord findDiskRecordWithName:r.name inArray:_tableViewDataSourceLocal];
        if (!r) {
            r = _tableViewDataSourceMerged[row];
        }
    }
    cellView.textField.stringValue = [NSString stringWithFormat:@"%@,%@", r.name, r.identifier];
    [self handleTextColorForCellView:cellView withDiskRecord:r];
    [self handleDeselectRow];
    return cellView;
}

-(BOOL)tableView:(NSTableView *)tableView shouldSelectRow:(NSInteger)row {
    _selectedRow = row;
    
    return YES;
}

- (void)handleTextColorForCellView:(NSTableCellView *)cellView withDiskRecord:(DiskRecord *)r {
    bool foundRemote    = [self isRemoteObject:r];
    bool foundLocal     = [self isLocalObject:r];
    
    if (foundRemote && foundLocal) {
        cellView.textField.textColor = [NSColor greenColor];
    } else if (!foundRemote && foundLocal) {
        cellView.textField.textColor = [NSColor redColor];
    } else if (!foundRemote && !foundLocal) {
        cellView.textField.textColor = [NSColor blackColor];
    } else if (foundRemote && !foundLocal) {
        cellView.textField.textColor = [NSColor blackColor];
    }
}

- (void)handleDeselectRow {
    if (!_shouldDeselectTableViewRow) {
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:_selectedRow];
        
        [_tableView selectRowIndexes:indexSet byExtendingSelection:NO];
        _shouldDeselectTableViewRow = YES;
    }
}
@end
