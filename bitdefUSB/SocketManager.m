//
//  SocketManager.m
//  bitdefUSB
//
//  Created by Good on 18/05/2017.
//  Copyright © 2017 Good. All rights reserved.
//

#import "SocketManager.h"
#import "DiskRecord.h"

@interface SocketManager () <GCDAsyncSocketDelegate>
@property (nonatomic, copy) void (^completionHandler)(NSDictionary *data, NSError *error);
@property (nonatomic, copy) void (^completionHandlerDisconnect)(NSError *error);
@property (strong) GCDAsyncSocket *asyncSocket;

@end

@implementation SocketManager

+ (id)sharedInstance {
    static SocketManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [self new];
    });
    return sharedMyManager;
}

- (void) connectToServer:(void (^)(NSDictionary* data, NSError* error))completionHandler
                        :(void (^)(NSError* error))completionHandlerDisconnect{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        self.completionHandler = completionHandler;
        self.completionHandlerDisconnect = completionHandlerDisconnect;
        dispatch_queue_t mainQueue = dispatch_get_main_queue();
        
        _asyncSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:mainQueue];
        _asyncSocket.delegate = self;
        NSString *host = @"192.168.0.102";
        uint16_t port = 5555;
        NSError *error = nil;
        if (![_asyncSocket connectToHost:host onPort:port error:&error])
        {
            NSLog(@"Error connecting: %@", error.description);
            self.completionHandler(nil,error);
        }
    });
}

- (void) fetchDataFromServer:(void (^)(NSDictionary* data,NSError* error))completionHandler
                            :(void (^)(NSError* error))completionHandlerDisconnect {
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        self.completionHandlerDisconnect = completionHandlerDisconnect;
        self.completionHandler = completionHandler;
        
        if (_asyncSocket != nil) {
            [self writeData:@"{\"cmd\":\"get\"}"];
        } else {
            [self connectToServer:^(NSDictionary *data, NSError *error) {
                if (error) {
                    completionHandler(nil,error);
                }
                [self fetchDataFromServer:^(NSDictionary *data, NSError *error) {
                    completionHandler(data,error);
                } :^(NSError *error) {
                    completionHandler(nil,error);
                }];
                
            } :^(NSError *error) {
                completionHandler(nil,error);
            }];
        }
        
    });
}

- (void) addDataToServer:(DiskRecord *)data
                        :(void (^)(NSDictionary* data,NSError* error))completionHandler
                        :(void (^)(NSError* error))completionHandlerDisconnect {
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        self.completionHandlerDisconnect = completionHandlerDisconnect;
        self.completionHandler = completionHandler;
        
        if (_asyncSocket != nil) {
//            NSArray *arr  = @[@{@"volID":data.identifier},@{@"volName":data.name}];
//            NSDictionary* dictionary = @{@"cmd":@"add",@"data":arr};
//            NSError *error;
//            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
//                                                               options:0
//                                                                 error:&error];
//            NSString *stringWithoutSpaces = [[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding ]
//                                             stringByReplacingOccurrencesOfString:@" " withString:@""];
            //server only accepts malformed json. after data there should be an array identifier.
            NSString *string = [NSString stringWithFormat:@"{\"cmd\":\"add\",\"data\":{\"voldID\":\"%@\",\"volName\":\"%@\"}}",data.identifier,data.name];
            [self writeData:string];
            //for multiple additions.
//            for (DiskRecord *r in data) {
//                [string stringByAppendingString:[NSString stringWithFormat:@"\"voldId\":\"%@\",\"volName\":\"%@\"",r.identifier,r.name]];
//                [string stringByAppendingString:@","];
//            }
//            string = [string substringToIndex:[string length]-1];
//            [string stringByAppendingString:@"}}"];
            
        }
    });
}

- (void) disconnectFromServer:(void (^)(NSDictionary* data,NSError* error))completionHandler
                             :(void (^)(NSError* error))completionHandlerDisconnect {
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        self.completionHandler = completionHandler;
        self.completionHandlerDisconnect = completionHandlerDisconnect;
        if (_asyncSocket != nil) {
            [self writeData:@"{\"cmd\":\"disconnect\"}"];
        }
        
        [_asyncSocket disconnectAfterReadingAndWriting];
    });
}

- (void)deleteDiskRecordFromServer: (DiskRecord *)data
                                  :(void (^)(NSDictionary* data,NSError* error))completionHandler
                                  :(void (^)(NSError* error))completionHandlerDisconnect {
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        self.completionHandler = completionHandler;
        self.completionHandlerDisconnect = completionHandlerDisconnect;
        if (_asyncSocket != nil) {
            [self writeData:[NSString stringWithFormat:@"{\"cmd\":\"delete\",\"data\":{\"voldID\":\"%@\",\"volName\":\"%@\"}}",data.identifier,data.name]];
            
            //workaround to delete null id from server.
            [self writeData:[NSString stringWithFormat:@"{\"cmd\":\"delete\",\"data\":{\"voldID\":\"null\",\"volName\":\"%@\"}}",data.name]];
        }
    });
}

- (void) writeData:(NSString *)string {
    NSLog(@"%@",string);
    [_asyncSocket writeData:[string dataUsingEncoding:NSUTF8StringEncoding] withTimeout:5 tag:1];
    NSData *responseTerminatorData = [@"\n" dataUsingEncoding:NSASCIIStringEncoding];
    
    [_asyncSocket readDataToData:responseTerminatorData withTimeout:-1.0 tag:0];
}

#pragma mark Socket Delegate
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port {
    NSLog(@"%@",host);
    _asyncSocket = sock;

    [self writeData:@"{\"cmd\":\"connect\"}"];
}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag {
    NSData *strData = [data subdataWithRange:NSMakeRange(0, [data length] - 2)];
    NSString *msg = [[NSString alloc] initWithData:strData encoding:NSUTF8StringEncoding];
    if(msg) {
        NSLog(@"%@",msg);
        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:strData options:0 error:nil];
        NSInteger ss = [jsonObject[@"error"] integerValue];
        if (ss == 0) {
            self.completionHandler(jsonObject,nil);
        } else {
            self.completionHandler(nil,[NSError errorWithDomain:@"idk" code:0 userInfo:@{@"omg":@"ponies"}]);
        }
        
    } else {
        self.completionHandler(nil,[NSError errorWithDomain:@"idk" code:1 userInfo:@{@"omg ponies":@"ponies"}]);
    }
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err {
    self.completionHandlerDisconnect(err);
}

@end
