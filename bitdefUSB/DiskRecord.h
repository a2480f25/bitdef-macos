//
//  DiskRecord.h
//  bitdefUSB
//
//  Created by Good on 17/05/2017.
//  Copyright © 2017 Good. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DiskRecord : NSObject
- (instancetype) init __attribute__((unavailable("init not available")));
- (instancetype) initWithDictionary:(NSDictionary*)d;
- (instancetype) initWithRemoteDictionary:(NSDictionary *)d;

+ (void) addUniqueDiskRecord:(DiskRecord *)r toArray:(NSMutableArray *)a;
+ (BOOL) isObject:(DiskRecord *)r presentIn:(NSMutableArray *)a;
+ (void) removeObject:(DiskRecord *)rr fromArray:(NSMutableArray *)a;
+ (DiskRecord *) findDiskRecordWithName:(NSString *)s inArray:(NSArray *)a;

@property (strong) NSString* identifier;
@property (strong) NSString* uuidKey;
@property (strong) NSString* mediaName;
@property (strong) NSString* mediaUUIDKey;
@property (strong) NSString* deviceGUIDKey;
@property (strong) NSString* bsdName;
@property (strong) NSString* name;
@property (strong) NSString* volumeURL;
@property (strong) NSString* path;
@property (atomic) BOOL mounted;
@property (strong) NSString* writable;
@property (strong) NSString* diskSize;
@property (strong) NSString* freeSpace;

@end
